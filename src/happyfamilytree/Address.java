/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package happyfamilytree;

import java.util.Objects;

/**
 * TODO Doc.
 */
public class Address {
    private int streetNumber;
    private String streetName;
    private String suburb;
    private String postcode;

    public int getStreetNumber() {
        return streetNumber;
    }

    public void setStreetNumber(int streetNumber) {
        this.streetNumber = streetNumber;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public String getSuburb() {
        return suburb;
    }

    public void setSuburb(String suburb) {
        this.suburb = suburb;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Address address = (Address) o;
        return streetNumber == address.streetNumber
                && Objects.equals(streetName, address.streetName)
                && Objects.equals(suburb, address.suburb)
                && Objects.equals(postcode, address.postcode);
    }

    @Override
    public int hashCode() {
        return  Objects.hash(streetNumber, streetName, suburb, postcode);
    }
}
