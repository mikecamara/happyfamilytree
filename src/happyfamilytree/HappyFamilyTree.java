/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package happyfamilytree;

import java.awt.Color;
import java.awt.Component;
import java.awt.GridLayout;
import java.awt.Container;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.Box;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import java.util.Vector;//imports vector utility
import javax.swing.JFileChooser;
import static javax.swing.JFileChooser.SAVE_DIALOG;
import javax.swing.SwingConstants;

/**
 *
 * @author mcmikecamara
 */
public class HappyFamilyTree extends JFrame implements ActionListener {

    private final JButton button1, button2, buttonEdit, buttonSave, buttonLoadFile, buttonCreatePerson, buttonShowFTree; // array of buttons
    private boolean toggle = true; // toggle between two layouts
    private final Container container; // frame container
    private final GridLayout gridLayout2; // second gridl

    private String surname = null;

    private JComboBox<String> listOfFamiliesComboBox; // holds icon names

    private final JLabel labelName0, labelSurname0, labelBirthSurname0, labelGender0,
            labelTitle0, labelAddress0, labelDescription0, labelFather0,
            labelMother0, labelChildren0; // displays selected icon

    Vector<String> myVector = new Vector<String>(1, 1);
    private ArrayList<Map> familiesList = new ArrayList<Map>();

    private int counter, famNumber, token;
    int counterFamilyInner = 0;

    int counterElemFamily = 0;

    int counterComboBox = 0;

    int counterr = 0;

    private String family;

    private final JTextField textFieldTitle, textFieldName, textFieldSurname, textFieldBirthSurname,
            textFieldGender, textFieldAddress, textFieldFather, textFieldMother,
            textFieldChildren;

    private final JTextArea textAreaDescription;
    
    Map<String, String> person1;

    JFileChooser fc;
    private JLabel infoLabel;
    private JTextArea myarea;
    private JButton btnOpen;
    private JButton btnSave;
    private JButton btnSaveAs;
    private JButton btnClose;
    private String line;
    private String filePath;
    private BufferedReader in;
    JScrollPane scrollPane;

    // Method to open the file
    private String selectFile() {
        String dataFileText = null;
        JFileChooser fc = new JFileChooser();

        int returnVal = fc.showOpenDialog(this);

        if (returnVal == JFileChooser.APPROVE_OPTION) {
            File file = fc.getSelectedFile();
            if (file == null) {
                return (dataFileText);
            }
            dataFileText = file.getAbsolutePath();
        } else {
            return (dataFileText);
        }
        return (dataFileText);
    }// end select file method
    
    public void addPersonToArrayList(Map e)
    {
        familiesList.add(e);
    }
    
    
    public void addSurnameToMenu(String s)
    {
           for (int i = 0; i < familiesList.size(); i++) {

                        // I have a vector myVector that get the surname of the current iterate map (person)
                        // The first comparison is if myVector already have the surname
                        if (myVector.contains(s)) {

                        } 
                        // if it is not in the vector, in other words new
                        // add the surname to the list of surnamens myVector
                        else {
                            myVector.add(s);
                            System.out.println(myVector.size());
                        }
                    }
    }
    
    
    class OpenButtonListener implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            filePath = selectFile();
            if ((filePath != null)) {
                try {
                    
                    // Creates a map that is actually a person
                    //Map<String, String> person1 = new HashMap<String, String>();

                    int counterMaps = 0;

                    BufferedReader in = new BufferedReader(new FileReader(filePath));
                    String key, value;
                    //   myarea.setText("");
                        
                    line = in.readLine();
                    
                    //loop through all lines of text file chosen (family.txt)
                    while (line != null) {
                        
                        person1 = new HashMap<String, String>();
                        
                        String[] tokens = line.split(";");
                        value = tokens[0];
                        person1.put("title", value);

                        value = tokens[1];
                        person1.put("name", value);

                        value = tokens[2];
                        person1.put("surname", value);

                        value = tokens[3];
                        person1.put("birthSurname", value);

                        value = tokens[4];
                        person1.put("gender", value);

                        value = tokens[5];
                        person1.put("address", value);

                        value = tokens[6];
                        person1.put("description", value);

                        value = tokens[7];
                        person1.put("father", value);

                        value = tokens[8];
                        person1.put("mother", value);

                        value = tokens[9];
                        person1.put("children", value);

                        value = tokens[10];
                        person1.put("id", value);

                        // Here is the problem
                        // at th moment every time that it loops it is
                        // deleting the previous person
                        // I have to find a way to change this person1
                        // to something else
                        // at the moment I have 8 copies of the same object
                        
                        familiesList.add(counterMaps, person1);

                        // myarea.append(line + "\n");
                       
                        System.out.println(familiesList.get(counterMaps).get("surname").toString() + "Surname in the map added to arrayList");

                        counterMaps++;

                        line = in.readLine();
                    }
                    in.close();// finish reading the file

                    // Loops all maps (persons) stored in the arraylist
                    for (int i = 0; i < familiesList.size(); i++) {

                        // I have a vector myVector that get the surname of the current iterate map (person)
                        // The first comparison is if myVector already have the surname
                        if (myVector.contains(familiesList.get(i).get("surname").toString())) {

                        } 
                        // if it is not in the vector, in other words new
                        // add the surname to the list of surnamens myVector
                        else {
                            myVector.add(familiesList.get(i).get("surname").toString());
                        }
                    }

                    // this is the combobox that have the list of families
                    listOfFamiliesComboBox.setMaximumRowCount(myVector.size()); // display rows according to number of families

                    listOfFamiliesComboBox.addItemListener(new ItemListener() // anonymous inner class
                    {
                        // handle JComboBox event
                        @Override
                        public void itemStateChanged(ItemEvent event) {
                            // determine whether item selected
                            if (event.getStateChange() == ItemEvent.SELECTED) {

                                // family variable becomes the same as chose family from menu combobox
                                family = listOfFamiliesComboBox.getSelectedItem().toString();
                                
                                // Loop though all person in the arrayList
                                for (int i = 0; i < familiesList.size(); i++) {
                                    
                                    // check if iterate person surname equals to family selected
                                    if (familiesList.get(i).get("surname").toString().equals(family)) {

                                        counterElemFamily++;
                                        
                                        // compare iterator title
                                        // if "dad" is present it will print the 
                                        // father figure of thr family in the UI
                                        if (familiesList.get(i).get("title").toString().equals("dad")) {
                                            textFieldTitle.setText(familiesList.get(i).get("title").toString());
                                            textFieldTitle.setEditable(false);
                                            textFieldName.setText(familiesList.get(i).get("name").toString());
                                            textFieldName.setEditable(false);
                                            textFieldSurname.setText(familiesList.get(i).get("surname").toString());
                                            textFieldSurname.setEditable(false);
                                            textFieldBirthSurname.setText(familiesList.get(i).get("birthSurname").toString());
                                            textFieldBirthSurname.setEditable(false);
                                            textFieldGender.setText(familiesList.get(i).get("gender").toString());
                                            textFieldGender.setEditable(false);
                                            textFieldAddress.setText(familiesList.get(i).get("address").toString());
                                            textFieldAddress.setEditable(false);
                                            textAreaDescription.setText(familiesList.get(i).get("description").toString());
                                            textAreaDescription.setEditable(false);
                                            textFieldFather.setText(familiesList.get(i).get("father").toString());
                                            textFieldFather.setEditable(false);
                                            textFieldMother.setText(familiesList.get(i).get("mother").toString());
                                            textFieldMother.setEditable(false);
                                            textFieldChildren.setText(familiesList.get(i).get("children").toString());
                                            textFieldChildren.setEditable(false);

                                            counter = Integer.parseInt(familiesList.get(i).get("id").toString());

                                            famNumber = i;

                                            token = i;
                                        } // end if title == dad
                                    } // end if family -- surname 
                                } // end iterate all maps in arrayList 
                            } // end if event
                        } // end itemStateChanged
                    } // // end listOfFamiliesComboBox new ItemListener()
                    );// end addItemListener()

                    
                } catch (IOException a) {
                    System.out.println(a.getMessage());
                }
            } else if (null != filePath) {
                //  infoLabel.setText("Cannot open file " + filePath);
                //  myarea.setText("");
            }
        } // end actionPerformed(ActionEvent e) button open file
    } // end class open button listener

    
    class addPersonButtonListener implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            //labelName0.setText("that's " + (++counter));
            // String surname = JOptionPane.showInputDialog("Enter Family Surname");
            
            

            System.out.println("Big boy");

        }// end of method actionedPerformed addPersonButtonListener
    } // end of class addPersonButtonListener

    class createPersonButtonListener implements ActionListener {
        // inner class - implements the interface ActionListener

        public void actionPerformed(ActionEvent e) {

            JFrame CreatePerson = new JFrame();
                    new CreatePerson();
            
            buttonShowFTree.setEnabled(true);

            //String surname = JOptionPane.showInputDialog("Enter Family Surname");

//            if (surname == null || surname.equals(" ")) {
//
//            } else {
//
//                for (int i = 1; i < myVector.size(); i++) {
//
//                    if (surname.equals(familiesList.get(i).get("surname").toString())) {
//                        counterComboBox++;
//                    } else {
//
//                    }
//                }
//
//                if (counterComboBox == 0) {
//                    myVector.add(surname);
//                }
//
//                listOfFamiliesComboBox.updateUI();
//
//                textFieldTitle.setText("");
//                textFieldName.setText("");
//                textFieldSurname.setText(surname);
//                textFieldBirthSurname.setText("");
//                textFieldGender.setText("");
//                textFieldAddress.setText("");
//                textAreaDescription.setText("");
//                textFieldFather.setText("");
//                textFieldMother.setText("");
//                textFieldChildren.setText("");
//
//            }

        }// end of method actionedPerformed
    } // end of class MyButtonListener

// no-argument constructor
    public HappyFamilyTree() {
        super("GridLayout");

        gridLayout2 = new GridLayout(0, 2); // 3 by 2; no gaps
        container = getContentPane();
        container.setBackground(new Color(83, 83, 83));

        setLayout(gridLayout2);
        
        listOfFamiliesComboBox = new JComboBox<String>(myVector); // set up JComboBox
        listOfFamiliesComboBox.setMaximumRowCount(myVector.size()); // display 

        listOfFamiliesComboBox.addItemListener(new ItemListener() // anonymous inner class
        {
            // handle JComboBox event
            @Override
            public void itemStateChanged(ItemEvent event) {
                // determine whether item selected
                if (event.getStateChange() == ItemEvent.SELECTED) {

                    //labelComboBox.setIcon(icons[imagesJComboBox.getSelectedIndex()]);
                    family = listOfFamiliesComboBox.getSelectedItem().toString();
                    System.out.println(family);

                    for (int i = 0; i < familiesList.size(); i++) {

                        if (familiesList.get(i).get("surname").toString().equals(family)) {

                            counterElemFamily++;

                            if (familiesList.get(i).get("title").toString().equals("dad")) {
                                textFieldTitle.setText(familiesList.get(i).get("title").toString());
                                textFieldTitle.setEditable(false);
                                textFieldName.setText(familiesList.get(i).get("name").toString());
                                textFieldName.setEditable(false);
                                textFieldSurname.setText(familiesList.get(i).get("surname").toString());
                                textFieldSurname.setEditable(false);
                                textFieldBirthSurname.setText(familiesList.get(i).get("birthSurname").toString());
                                textFieldBirthSurname.setEditable(false);
                                textFieldGender.setText(familiesList.get(i).get("gender").toString());
                                textFieldGender.setEditable(false);
                                textFieldAddress.setText(familiesList.get(i).get("address").toString());
                                textFieldAddress.setEditable(false);
                                textAreaDescription.setText(familiesList.get(i).get("description").toString());
                                textAreaDescription.setEditable(false);
                                textFieldFather.setText(familiesList.get(i).get("father").toString());
                                textFieldFather.setEditable(false);
                                textFieldMother.setText(familiesList.get(i).get("mother").toString());
                                textFieldMother.setEditable(false);
                                textFieldChildren.setText(familiesList.get(i).get("children").toString());
                                textFieldChildren.setEditable(false);

                                counter = Integer.parseInt(familiesList.get(i).get("id").toString());

                                famNumber = i;

                                token = i;
                            }
                        }
                    }
                }
            } // end anonymous inner class
        }
        );// end addItemListener()

        buttonLoadFile = new JButton("Load File");
        OpenButtonListener openAction = new OpenButtonListener();
        buttonLoadFile.addActionListener(openAction);

        //buttonLoadFile.addActionListener(this); // register listener
        buttonLoadFile.setBackground(new Color(28, 28, 30));
        buttonLoadFile.setFont(new Font("Tahoma", Font.BOLD, 14));
        buttonLoadFile.setForeground(new Color(59, 59, 59));
        add(buttonLoadFile);

        add(listOfFamiliesComboBox); // add combo box to JFrame

        labelTitle0 = new JLabel("Title", SwingConstants.RIGHT);
        labelTitle0.setForeground(new Color(255, 255, 242));
        add(labelTitle0);
        textFieldTitle = new JTextField(10);
        add(textFieldTitle); // add textField1 to JFrame 27

        labelName0 = new JLabel("Name", SwingConstants.RIGHT);
        labelName0.setForeground(new Color(255, 255, 242));
        add(labelName0);
        textFieldName = new JTextField(10);
        add(textFieldName); // add textField1 to JFrame 27

        labelSurname0 = new JLabel("Surname", SwingConstants.RIGHT);
        labelSurname0.setForeground(new Color(255, 255, 242));
        add(labelSurname0);
        textFieldSurname = new JTextField(10);
        add(textFieldSurname);

        labelBirthSurname0 = new JLabel("Birth Surname", SwingConstants.RIGHT);
        labelBirthSurname0.setForeground(new Color(255, 255, 242));
        add(labelBirthSurname0);
        textFieldBirthSurname = new JTextField(10);
        add(textFieldBirthSurname);

        labelGender0 = new JLabel("Gender", SwingConstants.RIGHT);
        labelGender0.setForeground(new Color(255, 255, 242));
        add(labelGender0);
        textFieldGender = new JTextField(10);
        add(textFieldGender);

        labelAddress0 = new JLabel("Address", SwingConstants.RIGHT);
        labelAddress0.setForeground(new Color(255, 255, 242));
        add(labelAddress0);
        textFieldAddress = new JTextField(10);
        add(textFieldAddress);

        labelDescription0 = new JLabel("Description", SwingConstants.RIGHT);
        labelDescription0.setForeground(new Color(255, 255, 242));
        add(labelDescription0);

        textAreaDescription = new JTextArea("", 10, 15);
        add(new JScrollPane(textAreaDescription));

        labelFather0 = new JLabel("Father", SwingConstants.RIGHT);
        labelFather0.setForeground(new Color(255, 255, 242));
        add(labelFather0);
        textFieldFather = new JTextField(10);
        add(textFieldFather);

        labelMother0 = new JLabel("Mother", SwingConstants.RIGHT);
        labelMother0.setForeground(new Color(255, 255, 242));
        add(labelMother0);
        textFieldMother = new JTextField(10);
        add(textFieldMother);

        labelChildren0 = new JLabel("Children", SwingConstants.RIGHT);
        labelChildren0.setForeground(new Color(255, 255, 242));
        add(labelChildren0);
        textFieldChildren = new JTextField(10);
        add(textFieldChildren);

        button1 = new JButton("Previous");
        button1.addActionListener(this); // register listener

        button1.setBackground(new Color(28, 28, 30));
        button1.setFont(new Font("Tahoma", Font.BOLD, 14));
        button1.setForeground(new Color(59, 59, 59));
        add(button1);

        button2 = new JButton("Next");
        button2.addActionListener(this); // register listener

        button2.setBackground(new Color(28, 28, 30));
        button2.setFont(new Font("Tahoma", Font.BOLD, 14));
        button2.setForeground(new Color(59, 59, 59));
        add(button2);

        buttonEdit = new JButton("Edit");
        buttonEdit.addActionListener(this); // register listener

        buttonEdit.setBackground(new Color(28, 28, 30));
        buttonEdit.setFont(new Font("Tahoma", Font.BOLD, 14));
        buttonEdit.setForeground(new Color(59, 59, 59));
        add(buttonEdit);

        buttonSave = new JButton("Save");
        buttonSave.addActionListener(this); // register listener

        buttonSave.setBackground(new Color(28, 28, 30));
        buttonSave.setFont(new Font("Tahoma", Font.BOLD, 14));
        buttonSave.setForeground(new Color(59, 59, 59));
        add(buttonSave);
        buttonSave.setEnabled(false);

        buttonCreatePerson = new JButton("Create new Person");

        buttonCreatePerson.addActionListener(
                new createPersonButtonListener());

        buttonCreatePerson.setBackground(new Color(28, 28, 30));
        buttonCreatePerson.setFont(new Font("Tahoma", Font.BOLD, 14));
        buttonCreatePerson.setForeground(new Color(59, 59, 59));
        add(buttonCreatePerson);
        buttonCreatePerson.setEnabled(false);

        buttonShowFTree = new JButton("Show family tree");

        buttonShowFTree.addActionListener(this);

        buttonShowFTree.setBackground(new Color(28, 28, 30));
        buttonShowFTree.setFont(new Font("Tahoma", Font.BOLD, 14));
        buttonShowFTree.setForeground(new Color(59, 59, 59));
        add(buttonShowFTree);
        //buttonShowFTree.setEnabled(false);

        //FileDocumentListener evesDrop = new FileDocumentListener();
        // myarea.getDocument().addDocumentListener(evesDrop);
        getRootPane().setDefaultButton(buttonLoadFile);

        //MyButtonListener myAction = new MyButtonListener();
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        fc = new JFileChooser() {
            public void approveSelection() {
                File f = getSelectedFile();
                if (f.exists() && getDialogType() == SAVE_DIALOG) {
                    int result = JOptionPane.showConfirmDialog(this, f.getName() + " exists, overwrite?");
                    switch (result) {
                        case JOptionPane.YES_OPTION:
                            super.approveSelection();
                            return;

                        case JOptionPane.CANCEL_OPTION:
                            //  cancelSelection():
                            return;
                        default:
                            return;
                    }
                }
                super.approveSelection();
            }
        };

    }

// handle button events by toggling between layouts
    @Override
    public void actionPerformed(ActionEvent event) {

        container.validate(); // re-lay out container

        if (event.getSource() == button1) {
            if (counter > 1) {

                if (counter == counterElemFamily) {
                    button2.setEnabled(true);
                }

                counter--;

                if (counter == 1) {
                    button1.setEnabled(false);
                }
            }
        } else if (event.getSource() == button2) {

            if (counter < counterElemFamily) {
                counter++;

                if (counter == 2) {
                    button1.setEnabled(true);
                }

                if (counter == counterElemFamily) {
                    button2.setEnabled(false);
                }
            }

        } else if (event.getSource() == buttonEdit) {
            buttonCreatePerson.setEnabled(true);

            textFieldTitle.setEditable(true);
            textFieldName.setEditable(true);
            textFieldSurname.setEditable(true);
            textFieldBirthSurname.setEditable(true);
            textFieldGender.setEditable(true);
            textFieldAddress.setEditable(true);
            textAreaDescription.setEditable(true);
            textFieldFather.setEditable(true);
            textFieldMother.setEditable(true);
            textFieldChildren.setEditable(true);

            buttonSave.setEnabled(true);

        } else if (event.getSource() == buttonSave) {
            textFieldTitle.setEditable(true);

            System.out.println(textFieldName.getText());

            textFieldTitle.setEditable(false);
            textFieldName.setEditable(false);
            textFieldSurname.setEditable(false);
            textFieldBirthSurname.setEditable(false);
            textFieldGender.setEditable(false);
            textFieldAddress.setEditable(false);
            textAreaDescription.setEditable(false);
            textFieldFather.setEditable(false);
            textFieldMother.setEditable(false);
            textFieldChildren.setEditable(false);
            buttonSave.setEnabled(false);
            
            
            

            //Check if is new if is new create new person
            //otherwise just update as below
            if (familiesList.get(token).get("title").toString().equals(textFieldTitle.getText())) {

            } else {
                familiesList.get(token).replace("title", familiesList.get(token).get("title").toString(), textFieldTitle.getText());
            }

            if (familiesList.get(token).get("name").toString().equals(textFieldName.getText())) {

            } else {
                familiesList.get(token).replace("name", familiesList.get(token).get("name").toString(), textFieldName.getText());
            }

            if (familiesList.get(token).get("surname").toString().equals(textFieldSurname.getText())) {

            } else {
                familiesList.get(token).replace("surname", familiesList.get(token).get("surname").toString(), textFieldSurname.getText());
            }
            if (familiesList.get(token).get("birthSurname").toString().equals(textFieldBirthSurname.getText())) {

            } else {
                familiesList.get(token).replace("birthSurname", familiesList.get(token).get("birthSurname").toString(), textFieldBirthSurname.getText());
            }
            if (familiesList.get(token).get("gender").toString().equals(textFieldGender.getText())) {

            } else {
                familiesList.get(token).replace("gender", familiesList.get(token).get("gender").toString(), textFieldGender.getText());
            }
            if (familiesList.get(token).get("address").toString().equals(textFieldAddress.getText())) {

            } else {
                familiesList.get(token).replace("address", familiesList.get(token).get("address").toString(), textFieldAddress.getText());
            }
            if (familiesList.get(token).get("description").toString().equals(textAreaDescription.getText())) {

            } else {
                familiesList.get(token).replace("description", familiesList.get(token).get("description").toString(), textAreaDescription.getText());
            }
            if (familiesList.get(token).get("father").toString().equals(textFieldFather.getText())) {

            } else {
                familiesList.get(token).replace("father", familiesList.get(token).get("father").toString(), textFieldFather.getText());
            }
            if (familiesList.get(token).get("mother").toString().equals(textFieldMother.getText())) {

            } else {
                familiesList.get(token).replace("mother", familiesList.get(token).get("mother").toString(), textFieldMother.getText());
            }
            if (familiesList.get(token).get("children").toString().equals(textFieldChildren.getText())) {

            } else {
                familiesList.get(token).replace("children", familiesList.get(token).get("children").toString(), textFieldChildren.getText());
            }
            
            if (filePath != null) {
                try {
                    FileWriter writer = new FileWriter(filePath);
                    
                    for(int i = 0; i < familiesList.size(); i++){
                        
                        System.out.println("test");
                        
                        writer.write(familiesList.get(i).get("title").toString());
                        writer.write(";");
                        writer.write(familiesList.get(i).get("name").toString());
                        writer.write(";");
                        writer.write(familiesList.get(i).get("surname").toString());
                        writer.write(";");
                        writer.write(familiesList.get(i).get("birthSurname").toString());
                        writer.write(";");
                        writer.write(familiesList.get(token).get("gender").toString());
                        writer.write(";");
                        writer.write(familiesList.get(i).get("address").toString());
                        writer.write(";");
                        writer.write(familiesList.get(i).get("description").toString());
                        writer.write(";");
                        writer.write(familiesList.get(i).get("father").toString());
                        writer.write(";");
                        writer.write(familiesList.get(token).get("mother").toString());
                        writer.write(";");
                        writer.write(familiesList.get(token).get("children").toString());
                        writer.write(";");
                        writer.write(familiesList.get(token).get("children").toString());
                        writer.write("\n");
                    }
                    
                   
                    writer.flush();
                    writer.close();
                    buttonSave.setEnabled(false);
                    //btnClose.setEnabled(true);
                    
                } catch (IOException a) {
                    System.out.println(a.getMessage());
                }
            } else if (null != filePath) {
                //infoLabel.setText("Cannot open file " + filePath);
                //myarea.setText("");
            }
            
//            class SaveasButtonListener implements ActionListener {
//
//            public void actionPerformed(ActionEvent e) {
//            if (filePath != null) {
//                try {
//                    FileWriter writer = new FileWriter(filePath);
//                    
//                    for(int i = 0; i < familiesList.size(); i++){
//                        
//                        System.out.println("test");
//                        
//                        writer.write(familiesList.get(i).get("title").toString());
//                        writer.write(familiesList.get(i).get("name").toString());
//                        writer.write(familiesList.get(i).get("surname").toString());
//                        
//                        
//                    }
//                    
//                   
//                    writer.flush();
//                    writer.close();
//                    buttonSave.setEnabled(false);
//                    //btnClose.setEnabled(true);
//                    
//                } catch (IOException a) {
//                    System.out.println(a.getMessage());
//                }
//            } else if (null != filePath) {
//                //infoLabel.setText("Cannot open file " + filePath);
//                //myarea.setText("");
//            }
//            }
//            }

        } else if (event.getSource() == buttonShowFTree) {
            
             String s = " Name: "+ textFieldName.getText()+"\n Father: "+textFieldFather.getText()+
                     "\n Mother: "+textFieldMother.getText() + "\n Children: "+textFieldChildren.getText();
            JOptionPane.showMessageDialog(rootPane, s);
           // System.out.println(textFieldTitle.getText());

            //System.out.println(textFieldName.getText());
//            textFieldTitle.setText("");
//            textFieldName.setText("");
//            textFieldSurname.setText("");
//            textFieldBirthSurname.setText("");
//            textFieldGender.setText("");
//            textFieldAddress.setText("");
//            textAreaDescription.setText("");
//            textFieldFather.setText("");
//            textFieldMother.setText("");
//            textFieldChildren.setText("");
            System.out.println(textFieldTitle.getText());

            add(textFieldTitle);

            //textFieldChildren.
        }

        family = familiesList.get(famNumber).get("surname").toString();

        // loop all through all maps objects that I have contained in familiesList
        for (int i = 0; i < familiesList.size(); i++) {

            // compare key surname of the object i to family name selected
            if (familiesList.get(i).get("surname").toString().equals(family)) {

                counterFamilyInner++;

                // if is the same familily check for id == counter
                if (Integer.parseInt(familiesList.get(i).get("id").toString()) == counter) {

                    Container parent;

                    switch (counter) {
                        case 1:

                            textFieldTitle.setText(familiesList.get(i).get("title").toString());
                            textFieldTitle.setEditable(false);
                            textFieldName.setText(familiesList.get(i).get("name").toString());
                            textFieldSurname.setText(familiesList.get(i).get("surname").toString());
                            textFieldBirthSurname.setText(familiesList.get(i).get("birthSurname").toString());
                            textFieldGender.setText(familiesList.get(i).get("gender").toString());
                            textFieldAddress.setText(familiesList.get(i).get("address").toString());
                            textAreaDescription.setText(familiesList.get(i).get("description").toString());
                            textFieldFather.setText(familiesList.get(i).get("father").toString());
                            textFieldMother.setText(familiesList.get(i).get("mother").toString());
                            textFieldChildren.setText(familiesList.get(i).get("children").toString());

                            counterFamilyInner = 0;

                            token = i;

                            ;
                            break;
                        case 2:

                            textFieldTitle.setText(familiesList.get(i).get("title").toString());
                            textFieldName.setText(familiesList.get(i).get("name").toString());
                            textFieldSurname.setText(familiesList.get(i).get("surname").toString());
                            textFieldSurname.setText(familiesList.get(i).get("birthSurname").toString());
                            textFieldGender.setText(familiesList.get(i).get("gender").toString());
                            textFieldAddress.setText(familiesList.get(i).get("address").toString());
                            textAreaDescription.setText(familiesList.get(i).get("description").toString());
                            textFieldFather.setText(familiesList.get(i).get("father").toString());
                            textFieldMother.setText(familiesList.get(i).get("mother").toString());
                            textFieldChildren.setText(familiesList.get(i).get("children").toString());
                            counterFamilyInner = 0;

                            token = i;

                            ;
                            break;
                        case 3:

                            textFieldTitle.setText(familiesList.get(i).get("title").toString());
                            textFieldName.setText(familiesList.get(i).get("name").toString());
                            textFieldSurname.setText(familiesList.get(i).get("surname").toString());
                            textFieldBirthSurname.setText(familiesList.get(i).get("birthSurname").toString());
                            textFieldGender.setText(familiesList.get(i).get("gender").toString());
                            textFieldAddress.setText(familiesList.get(i).get("address").toString());
                            textAreaDescription.setText(familiesList.get(i).get("description").toString());
                            textFieldFather.setText(familiesList.get(i).get("father").toString());
                            textFieldMother.setText(familiesList.get(i).get("mother").toString());
                            textFieldChildren.setText(familiesList.get(i).get("children").toString());
                            counterFamilyInner = 0;

                            token = i;

                            ;
                            break;
                        case 4:

                            textFieldTitle.setText(familiesList.get(i).get("title").toString());
                            textFieldName.setText(familiesList.get(i).get("name").toString());
                            textFieldSurname.setText(familiesList.get(i).get("surname").toString());
                            textFieldBirthSurname.setText(familiesList.get(i).get("birthSurname").toString());
                            textFieldGender.setText(familiesList.get(i).get("gender").toString());
                            textFieldAddress.setText(familiesList.get(i).get("address").toString());
                            textAreaDescription.setText(familiesList.get(i).get("description").toString());
                            textFieldFather.setText(familiesList.get(i).get("father").toString());
                            textFieldMother.setText(familiesList.get(i).get("mother").toString());
                            textFieldChildren.setText(familiesList.get(i).get("children").toString());
                            counterFamilyInner = 0;

                            token = i;

                            ;
                            break;

                        default:
                            textFieldTitle.setText(familiesList.get(i).get("title").toString());
                            textFieldName.setText(familiesList.get(i).get("name").toString());
                            textFieldSurname.setText(familiesList.get(i).get("surname").toString());
                            textFieldBirthSurname.setText(familiesList.get(i).get("birthSurname").toString());
                            textFieldGender.setText(familiesList.get(i).get("gender").toString());
                            textFieldAddress.setText(familiesList.get(i).get("address").toString());
                            textAreaDescription.setText(familiesList.get(i).get("description").toString());
                            textFieldFather.setText(familiesList.get(i).get("father").toString());
                            textFieldMother.setText(familiesList.get(i).get("mother").toString());
                            textFieldChildren.setText(familiesList.get(i).get("children").toString());
                            counterFamilyInner = 0;

                            token = i;
                            ;
                            break;
                    }

                }
            }
        }
    }
    // end class GridLayoutFrame

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here

        HappyFamilyTree gridLayoutFrame = new HappyFamilyTree();
        gridLayoutFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        gridLayoutFrame.setSize(375, 647);
        gridLayoutFrame.setLocationRelativeTo(null);

        gridLayoutFrame.setVisible(true);
    }

}
