/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package happyfamilytree;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * TODO Doc
 */
public class FamilyMember {
    private final String name;
    private final String surname;
    private String surnameMarriage;
    private final String gender;
    private String biography;
    private Address address;
    private FamilyMember mother,father,spouse;
    private List<FamilyMember> children;

    public FamilyMember(String name, String surname, String gender, FamilyMember mother,FamilyMember father,FamilyMember spouse) {
        this.name = name;
        this.surname = surname;
        this.gender = gender;
        this.mother = mother;
        this.father = father;
        this.spouse = spouse;
    }
    
     public FamilyMember(String name, String surname, String gender){
        this(name,surname,gender,null,null,null);
    }
    
    public FamilyMember(String name, String surname, String gender,FamilyMember mother,FamilyMember father){
        this(name,surname,gender,mother,father,null);
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getSurnameMarriage() {
        return surnameMarriage;
    }

    public String getGender() {
        return gender;
    }

    public String getBiography() {
        return biography;
    }

    public Address getAddress() {
        return address;
    }

    public FamilyMember getMother() {
        return mother;
    }

    public FamilyMember getFather() {
        return father;
    }

    public FamilyMember getEspouse() {
        return spouse;
    }

    public List<FamilyMember> getChildren() {
        return children;
    }

    public void setSurnameMarriage(String surnameMarriage) {
        this.surnameMarriage = surnameMarriage;
    }

    public void setBiography(String biography) {
        this.biography = biography;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public void setFather(FamilyMember father) {
        this.father = father;
    }

    public void setEspouse(FamilyMember spouse) {
        this.spouse = spouse;
    }

    public void addChildren(FamilyMember child) {
        if (children == null) {
            children = new ArrayList<>();
        }
        this.children.add(child);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        FamilyMember that = (FamilyMember) o;
        return Objects.equals(name, that.name)
                && Objects.equals(surname, that.surname)
                && Objects.equals(surnameMarriage, that.surnameMarriage)
                && Objects.equals(gender, that.gender)
                && Objects.equals(biography, that.biography)
                && Objects.equals(address, that.address)
                && Objects.equals(mother, that.mother)
                && Objects.equals(father, that.father)
                && Objects.equals(spouse, that.spouse)
                && Objects.equals(children, that.children);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, surnameMarriage, gender, biography, address,
                mother, father, spouse, children);
    }
}