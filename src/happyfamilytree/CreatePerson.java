/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author mcmikecamara
 */
package happyfamilytree;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Map;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

public class CreatePerson extends JFrame {
    
    Map<String, String> person1;

    HappyFamilyTree hft = new HappyFamilyTree();
    
    private JButton saveCloseBtn = new JButton("Save Changes and Close");
    private JButton closeButton = new JButton("Exit Without Saving");
    private JFrame frame = new JFrame("Family tree");
    //private JTextArea textArea = new JTextArea();
    private JButton buttonSave, buttonCancel;

    private JTextField textFieldTitle, textFieldName, textFieldSurname, textFieldBirthSurname,
            textFieldGender, textFieldAddress, textFieldFather, textFieldMother,
            textFieldChildren;

    private JLabel labelName0, labelSurname0, labelBirthSurname0, labelGender0,
            labelTitle0, labelAddress0, labelDescription0, labelFather0,
            labelMother0, labelChildren0;
    private JTextArea textAreaDescription;

    public CreatePerson() {
        frame.setLocationRelativeTo(null);
        panels();
    }

    private void panels() {

        JPanel panel = new JPanel(new GridLayout(0, 2));
        panel.setBackground(new Color(83, 83, 83));
        panel.setBorder(new EmptyBorder(5, 5, 5, 5));
        //JPanel rightPanel = new JPanel(new GridLayout(15,0,10,10));
        //rightPanel.setBorder(new EmptyBorder(15, 5, 5, 10));
        // JScrollPane scrollBarForTextArea=new JScrollPane(textArea,JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        //panel.add(scrollBarForTextArea); 

        //frame.getContentPane().add(rightPanel,BorderLayout.EAST);
        //rightPanel.add(saveCloseBtn);
        //rightPanel.add(closeButton);
        frame.setSize(350, 550);
        frame.setVisible(true);
        frame.setLocationRelativeTo(null);

        labelTitle0 = new JLabel("Title", SwingConstants.RIGHT);
        labelTitle0.setForeground(new Color(255, 255, 242));
        panel.add(labelTitle0);
        textFieldTitle = new JTextField(10);
        panel.add(textFieldTitle); // add textField1 to JFrame 27

        labelName0 = new JLabel("Name", SwingConstants.RIGHT);
        labelName0.setForeground(new Color(255, 255, 242));
        panel.add(labelName0);
        textFieldName = new JTextField(10);
        panel.add(textFieldName); // add textField1 to JFrame 27

        labelSurname0 = new JLabel("Surname", SwingConstants.RIGHT);
        labelSurname0.setForeground(new Color(255, 255, 242));
        panel.add(labelSurname0);
        textFieldSurname = new JTextField(10);
        panel.add(textFieldSurname);

        labelBirthSurname0 = new JLabel("Birth Surname", SwingConstants.RIGHT);
        labelBirthSurname0.setForeground(new Color(255, 255, 242));
        panel.add(labelBirthSurname0);
        textFieldBirthSurname = new JTextField(10);
        panel.add(textFieldBirthSurname);

        labelGender0 = new JLabel("Gender", SwingConstants.RIGHT);
        labelGender0.setForeground(new Color(255, 255, 242));
        panel.add(labelGender0);
        textFieldGender = new JTextField(10);
        panel.add(textFieldGender);

        labelAddress0 = new JLabel("Address", SwingConstants.RIGHT);
        labelAddress0.setForeground(new Color(255, 255, 242));
        panel.add(labelAddress0);
        textFieldAddress = new JTextField(10);
        panel.add(textFieldAddress);

        labelDescription0 = new JLabel("Description", SwingConstants.RIGHT);
        labelDescription0.setForeground(new Color(255, 255, 242));
        panel.add(labelDescription0);

        textAreaDescription = new JTextArea("", 10, 15);
        panel.add(new JScrollPane(textAreaDescription));

        labelFather0 = new JLabel("Father", SwingConstants.RIGHT);
        labelFather0.setForeground(new Color(255, 255, 242));
        panel.add(labelFather0);
        textFieldFather = new JTextField(10);
        panel.add(textFieldFather);

        labelMother0 = new JLabel("Mother", SwingConstants.RIGHT);
        labelMother0.setForeground(new Color(255, 255, 242));
        panel.add(labelMother0);
        textFieldMother = new JTextField(10);
        panel.add(textFieldMother);

        labelChildren0 = new JLabel("Children", SwingConstants.RIGHT);
        labelChildren0.setForeground(new Color(255, 255, 242));
        panel.add(labelChildren0);
        textFieldChildren = new JTextField(10);
        panel.add(textFieldChildren);

        buttonCancel = new JButton("Cancel");
        SaveNewPerson openAction = new SaveNewPerson();
        buttonCancel.addActionListener(openAction); // register listener

        buttonCancel.setBackground(new Color(28, 28, 30));
        buttonCancel.setFont(new Font("Tahoma", Font.BOLD, 14));
        buttonCancel.setForeground(new Color(59, 59, 59));
        panel.add(buttonCancel);

        buttonSave = new JButton("Save");
        buttonSave.addActionListener(openAction); // register listener

        buttonSave.setBackground(new Color(28, 28, 30));
        buttonSave.setFont(new Font("Tahoma", Font.BOLD, 14));
        buttonSave.setForeground(new Color(59, 59, 59));
        panel.add(buttonSave);

        frame.add(panel);

    }

    class SaveNewPerson implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent event) {

            if (event.getSource() == buttonCancel) {
                
                System.out.println("cancel");

            } else {
                System.out.println("saved");
                
                person1.put("title", textFieldTitle.getText());
                person1.put("name", textFieldName.getText());
                person1.put("surname", textFieldSurname.getText());
                person1.put("birthSurname", textFieldBirthSurname.getText());
                person1.put("gender", textFieldGender.getText());
                person1.put("address", textFieldAddress.getText());
                person1.put("description", textAreaDescription.getText());
                person1.put("father", textFieldFather.getText());
                person1.put("mother", textFieldMother.getText());
                person1.put("children", textFieldChildren.getText());
               // person1.put("id", value);
               
                // add person to ArrayList familiesList
                
                hft.addPersonToArrayList(person1);
                
                hft.addSurnameToMenu(textFieldSurname.getText());
                
                //if it is a new surname add to the combobox menu
            }
        }
    }
    
    

}
