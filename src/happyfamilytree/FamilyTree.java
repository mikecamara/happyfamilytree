/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package happyfamilytree;

import java.util.Objects;

/**
 *
 * @author mcmikecamara
 */
public class FamilyTree {
    private String familyName;
    private FamilyMember patriarch;

    public FamilyTree(String familyName, FamilyMember patriarch) {
        this.familyName = familyName;
        this.patriarch = patriarch;
    }

    public String getFamilyName() {
        return familyName;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    public FamilyMember getPatriarch() {
        return patriarch;
    }

    public void setPatriarch(FamilyMember patriarch) {
        this.patriarch = patriarch;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        FamilyTree that = (FamilyTree) o;
        return Objects.equals(familyName, that.familyName)
                && Objects.equals(patriarch, that.patriarch);
    }

    @Override
    public int hashCode() {
        return Objects.hash(familyName, patriarch);
    }
}